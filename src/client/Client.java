package client;

import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Client extends JFrame {

    private JTextField userText;
    private JTextArea chatWindow;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private String message = "";
    private String serverIP;
    private Socket connection;

//Konstruktor
    public Client(String host) {
        super("Client.");
        serverIP = host;
        userText = new JTextField();
        userText.setEditable(false);
        userText.addActionListener(
                new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                sendMessage(event.getActionCommand());
                userText.setText("");
            }
        }
        );
        add(userText, BorderLayout.NORTH);
        chatWindow = new JTextArea();
        add(new JScrollPane(chatWindow), BorderLayout.CENTER);
        setSize(300, 150);
        setVisible(true);
    }

    //Anslut till servern
    public void startRunning() {
        try {
            connectToServer();
            setupStreams();
            whileChatting();
        } catch (EOFException eofException) {
            showMessage("\n Clienten avslutade anslutningen");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            closeFunctions();
        }
    }

//Anslut till servern
    private void connectToServer() throws IOException {
        showMessage("Försöker ansluta...");
        connection = new Socket(InetAddress.getByName(serverIP), 6789);
        showMessage("Ansluten till: " + connection.getInetAddress().getHostName());
    }

    //Upprätt streams för att sända och ta emot meddelanden
    private void setupStreams() throws IOException {
        output = new ObjectOutputStream(connection.getOutputStream());
        output.flush();
        input = new ObjectInputStream(connection.getInputStream());
        showMessage("\n Streams är nu upprättade! \n");
    }

    //Medans man chattar mot servern
    private void whileChatting() throws IOException {
        ableToType(true);
        do {
            try {
                message = (String) input.readObject();
                showMessage("\n" + message);
            } catch (ClassNotFoundException classNotFoundException) {
                showMessage("Okänd data mottagen.");
            }
        } while (!message.equals("SERVER - END"));

    }

    //Stänger ner streams och sockets
    private void closeFunctions() {
        showMessage("\n Avslutar...");
        ableToType(false);
        try {
            output.close();
            input.close();
            connection.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    //Skicka meddelande till servern
    private void sendMessage(String message) {
        try {
            output.writeObject("CLIENT - " + message);
            output.flush();
            showMessage("\n CLIENT - " + message);
        } catch (IOException ioException) {
            chatWindow.append("\n Ett problem uppstod när meddelandet skickades.");
        }

    }

    //Ändra eller uppdatera chattrutan
    private void showMessage(final String m) {
        SwingUtilities.invokeLater(
                new Runnable() {
            public void run() {
                chatWindow.append(m);
            }
        }
        );
    }

    //Ger rättighet att skriva saker i chattrutan
    private void ableToType(final boolean tof) {
        SwingUtilities.invokeLater(
                new Runnable() {
            public void run() {
                userText.setEditable(tof);
            }
        }
        );
    }

}
